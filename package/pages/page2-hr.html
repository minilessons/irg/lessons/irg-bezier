<p>Na prethodnoj stranici izveli smo izraz za parametarski zapis aproksimacijske Bezierove krivulje čime smo došli do Bernsteinovih težinskih funkcija. U računalu, krivulju nećemo crtati na taj način (i računati svaki puta binomne koeficijente), već ćemo izračune prikazati matrično.</p>

<p>Uzmimo za primjer aproksimacijsku Bezierovu krivulju trećeg stupnja. Ona je određena izrazom:
\[
\vec p(t) = (1-t)^3 \cdot \vec r_0 + 3 \cdot (1-t)^2 t \cdot \vec r_1 + 3 \cdot (1-t) t^2 \cdot \vec r_2 + t^3 \cdot \vec r_3
\]
U 2D prostoru, to znači da <em>x</em> i <em>y</em> koordinate točaka krivulje računamo prema:
\[
\begin{align*}
p_x(t) &amp;= (1-t)^3 \cdot r_{0,x} + 3 \cdot (1-t)^2 t \cdot r_{1,x} + 3 \cdot (1-t) t^2 \cdot r_{2,x} + t^3 \cdot r_{3,x}\\
p_y(t) &amp;= (1-t)^3 \cdot r_{0,y} + 3 \cdot (1-t)^2 t \cdot r_{1,y} + 3 \cdot (1-t) t^2 \cdot r_{2,y} + t^3 \cdot r_{3,y}
\end{align*}
\]
Primijetite da <em>x</em>-koordinatu točke dobivamo kao skalarni produkt vektora čije su komponente vrijednosti težinskih funkcija i vektora čije su komponente <em>x</em>-koordinate zadanih točaka. Slična tvrdnja vrijedi i za <em>y</em>-koordinate. Uz zapis skalarnog produkta matrično, možemo pisati:
\[
  \begin{align*}
  p_x(t) &amp;= \left[ \begin{array}{cccc} (1-t)^3 &amp; 3 (1-t)^2 t &amp; 3 (1-t)^2 t &amp; t^3 \end{array} \right]
  \cdot
  \left[ \begin{array}{c} r_{0,x} \\ r_{1,x} \\ r_{2,x} \\ r_{3,x} \end{array} \right] \\
  p_y(t) &amp;= \left[ \begin{array}{cccc} (1-t)^3 &amp; 3 (1-t)^2 t &amp; 3 (1-t) t^2 &amp; t^3 \end{array} \right]
  \cdot
  \left[ \begin{array}{c} r_{0,y} \\ r_{1,y} \\ r_{2,y} \\ r_{3,y} \end{array} \right]
  \end{align*}
\]
Ako se dogovorimo da ćemo točke prikazivati kao vektor-retke (jednoretčane matrice), tada je \(\vec p(t) = \left[ \begin{array}{cc} p_x &amp; p_y \end{array} \right]\) pa prethodno kompaktnije možemo pisati kao jedan matrični umnožak:
\[
  \vec p(t) = \left[ \begin{array}{cc} p_x &amp; p_y \end{array} \right] = 
  \left[ \begin{array}{cccc} (1-t)^3 &amp; 3 (1-t)^2 t &amp; 3 (1-t) t^2 &amp; t^3 \end{array} \right]
  \cdot
  \left[ \begin{array}{cc} r_{0,x} &amp; r_{0,y} \\ r_{1,x} &amp; r_{1,y} \\ r_{2,x} &amp; r_{2,y} \\ r_{3,x} &amp; r_{3,y} \end{array} \right]
\]
(množimo matricu \(1 \times 4\) matricom \(4 \times 2\) i dobivamo matricu \(1 \times 2\) - koordinate točke.
</p>

<p>Sljedeći korak prema konačnom obliku jest korak u kojem jednoretčanu matricu vrijednosti težinskih funkcija želimo pojednostavniti na način da se pojavljuju samo i isključivo potencije parametra. Prvi korak jest svaku od komponenti ispotencirati do kraja i svesti na sumu potencija. Vrijedi:
\[
\begin{align}
 (1-t)^3 &amp;= -1 t^3 + 3 t^2 -3 t + 1 \\
 3 (1-t)^2 t &amp;= 3 t^3 - 6 t^2 +3 t + 0 \\
 3 (1-t) t^2 &amp;= -3 t^3 + 3 t^2 + 0 t + 0 \\ 
 t^3 &amp;=  1 t^3 + 0 t^2 + 0 t + 0 
\end{align}
\]
Uočite sada: svaki se rastav može zapisati kao skalarni produkt potencija vektora čije su komponente potencije parametra i vektora pripadnih koeficijenata.
\[
  \begin{align*}
  (1-t)^3 &amp;= \left[ \begin{array}{cccc} t^3 &amp; t^2 &amp; t &amp; 1 \end{array} \right]
  \cdot
  \left[ \begin{array}{c} -1 \\ 3 \\ -3 \\ 1 \end{array} \right] \\
  3 (1-t)^2 t &amp;= \left[ \begin{array}{cccc} t^3 &amp; t^2 &amp; t &amp; 1 \end{array} \right]
  \cdot
  \left[ \begin{array}{c} 3 \\ -6 \\ 3 \\ 0 \end{array} \right] \\
  3 (1-t) t^2 &amp;= \left[ \begin{array}{cccc} t^3 &amp; t^2 &amp; t &amp; 1 \end{array} \right]
  \cdot
  \left[ \begin{array}{c} -3 \\ 3 \\ 0 \\ 0 \end{array} \right] \\
  t^3 &amp;= \left[ \begin{array}{cccc} t^3 &amp; t^2 &amp; t &amp; 1 \end{array} \right]
  \cdot
  \left[ \begin{array}{c} 1 \\ 0 \\ 0 \\ 0 \end{array} \right] \\
  \end{align*}
\]
Sada početni vektor vrijednosti težinskih funkcija možemo zapisati kao:
\[
  \left[ \begin{array}{cccc} (1-t)^3 &amp; 3 (1-t)^2 t &amp; 3 (1-t) t^2 &amp; t^3 \end{array} \right] =
  \left[ \begin{array}{cccc} t^3 &amp; t^2 &amp; t &amp; 1 \end{array} \right]
  \cdot
  \left[ \begin{array}{cccc} -1 &amp; 3 &amp; -3 &amp; 1 \\ 3 &amp; -6 &amp; 3 &amp; 0 \\ -3 &amp; 3 &amp; 0 &amp; 0 \\ 1 &amp; 0 &amp; 0 &amp; 0 \end{array} \right]
\]
čime dolazimo do konačnog matričnog oblika:
\[
  \vec p(t) = 
\left[ \begin{array}{cccc} t^3 &amp; t^2 &amp; t &amp; 1 \end{array} \right]
  \cdot
  \left[ \begin{array}{cccc} -1 &amp; 3 &amp; -3 &amp; 1 \\ 3 &amp; -6 &amp; 3 &amp; 0 \\ -3 &amp; 3 &amp; 0 &amp; 0 \\ 1 &amp; 0 &amp; 0 &amp; 0 \end{array} \right]
  \cdot
  \left[ \begin{array}{cc} r_{0,x} &amp; r_{0,y} \\ r_{1,x} &amp; r_{1,y} \\ r_{2,x} &amp; r_{2,y} \\ r_{3,x} &amp; r_{3,y} \end{array} \right]
\]
 što kraće zapisujemo kao:
\[
  \vec p(t) = \mathcal{T}(t) \cdot \textbf{B}_3 \cdot \textbf{R}
\]
pri čemu je:</p>

<ul>
<li>\(\mathcal{T}(t)\) jednoretčani vektor (matrica \(1 \times (n+1)\)) potencija parametra,</li>
<li>\(\textbf{B}_n\) kvadratna matrica \((n+1) \times (n+1)\) koja odgovara aproksimacijskoj Bezierovoj krivulji stupnja <em>n</em> te</li>
<li>\(\textbf{R}\) matrica zadanih točaka (točke su po retcima).</li>
</ul>

<p>Matricu \(\textbf{B}_n\), za zadani \(n\), programski je moguće generirati relativno jednostavno, pa se ovaj opći zapis krivulje može koristiti za postupak crtanja. Pri tome korisnik predaje \(n+1\) točku čime se generira matrica \(\textbf{R}\), programski se stvara matrica \(\textbf{B}_n\) i potom se računa umnožak \(\textbf{K} = \textbf{B}_n \cdot \textbf{R}\). Jednom kad je matrica \(\textbf{K}\) dobivena, krivulja se uzorkuje za određen broj parametara (primjerice, za \(t=0\) do \(t=1\) uz korak 0.01, za svaki se \(t\) izračuna \(\mathcal{T}(t) \cdot \textbf{K}\) i dobivene se točke spoje linijskim segmentima. Ako se ovo učini dovoljno gusto, dobit će se prikaz koji izgleda kontinuirano.</p>

<p>Primijetite da za aproksimacijsku Bezierovu krivulju stupnja \(n\) moramo imati \(n+1\) točku. Matrice \(\textbf{B}_n\), za \(n \in \{1,2,3\}\) prikazane su u nastavku.</p>

\[
\begin{align*}
\textbf{B}_1 &amp;= \left[ \begin{array}{cc} -1 &amp; 1 \\ 1 &amp; 0 \end{array} \right]\\
\textbf{B}_2 &amp;= \left[ \begin{array}{cccc} 1 &amp; -2 &amp; 1 \\ -2 &amp; 2 &amp; 0 \\ 1 &amp; 0 &amp; 0 \end{array} \right]\\
\textbf{B}_3 &amp;= \left[ \begin{array}{cccc} -1 &amp; 3 &amp; -3 &amp; 1 \\ 3 &amp; -6 &amp; 3 &amp; 0 \\ -3 &amp; 3 &amp; 0 &amp; 0 \\ 1 &amp; 0 &amp; 0 &amp; 0 \end{array} \right]\\
\end{align*}
\] 

Time se dobivaju aproksimacijske Bezierove krivulje prvog, drugog i trećeg stupnja:

\[
\begin{align*}
  \vec p(t) &amp;= 
\left[ \begin{array}{cc} t &amp; 1 \end{array} \right]
  \cdot
  \left[ \begin{array}{cc} -1 &amp; 1 \\ 1 &amp; 0 \end{array} \right]
  \cdot
  \left[ \begin{array}{cc} r_{0,x} &amp; r_{0,y} \\ r_{1,x} &amp; r_{1,y} \end{array} \right] \\
  \vec p(t) &amp;= 
\left[ \begin{array}{ccc} t^2 &amp; t &amp; 1 \end{array} \right]
  \cdot
  \left[ \begin{array}{ccc} 1 &amp; -2 &amp; 1 \\ -2 &amp; 2 &amp; 0 \\ 1 &amp; 0 &amp; 0 \end{array} \right]
  \cdot
  \left[ \begin{array}{cc} r_{0,x} &amp; r_{0,y} \\ r_{1,x} &amp; r_{1,y} \\ r_{2,x} &amp; r_{2,y}\end{array} \right] \\
  \vec p(t) &amp;= 
\left[ \begin{array}{cccc} t^3 &amp; t^2 &amp; t &amp; 1 \end{array} \right]
  \cdot
  \left[ \begin{array}{cccc} -1 &amp; 3 &amp; -3 &amp; 1 \\ 3 &amp; -6 &amp; 3 &amp; 0 \\ -3 &amp; 3 &amp; 0 &amp; 0 \\ 1 &amp; 0 &amp; 0 &amp; 0 \end{array} \right]
  \cdot
  \left[ \begin{array}{cc} r_{0,x} &amp; r_{0,y} \\ r_{1,x} &amp; r_{1,y} \\ r_{2,x} &amp; r_{2,y} \\ r_{3,x} &amp; r_{3,y} \end{array} \right] \\
\end{align*}
\]

<p>Zanimljivo pitanje je sljedeće: "<i>možemo li Bezierovu krivulju definirati tako da prođe kroz (n+1) zadanu točku, umjesto samo kroz prvu i zadnju</i>"? Odgovor daje postupak kojim se konstruira interpolacijska Bezierova krivulja, a koji je opisan na sljedećoj stranici.</p>

