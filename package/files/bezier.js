  function binomial(n) {
    var triangle = [[1], [1, 1]];
    for(var i = 2; i <= n; i++) {
      var row = triangle[i-1];
      var l = row.length-1;
      var nrow = [1];
      for(var j = 0; j < l; j++) {
        nrow.push(row[j]+row[j+1]);
      }
      nrow.push(1);
      triangle.push(nrow);
    }
    this.calc = function(n,i) {
      return triangle[n][i];
    }
  }

  function bezierRMatrix(n) {
    var bnml = new binomial(n);

    function expand(k) {
      var a = [];
      var s = 1;
      for(var l = 0; l <= k; l++) {
        a.push(bnml.calc(k,l) * s);
        s = -s;
      }
      return a;
    }

    function zerosRow(k) {
      var a = [];
      for(var l = 0; l < k; l++) {
        a.push(0);
      }
      return a;
    }

    var rowMatrix = [];
    for(var ri = 0; ri <= n; ri++) {
      var arr = zerosRow(ri).concat(expand(n-ri));
      var c = bnml.calc(n,ri);
      for(var i = 0; i < arr.length; i++) {
        arr[i] = arr[i]*c;
      }
      arr = arr.reverse();
      rowMatrix.push(arr);
    }
    return math.transpose(math.matrix(rowMatrix));
  }

  function drawInterpolationBezier(canvasName) {
      this.canvas = document.getElementById(canvasName);
      this.context = this.canvas.getContext("2d");
      this.addingEnabled = true;
      this.curveApprox = false;
      this.curveInterpol = false;
      this.controlPolygon = false;
      this.controlPoints = true;
      this.intControlPolygon = false;
      this.intControlPoints = false;
      this.buildProc = false;
      this.tvalue = 0;
      this.showSimplerApprox = false;
      this.showTangentAtT = false;
      this.showTangentLineAtT = false;
      this.showTangentVectorAtT = false;
      this.scaleTangentVectorAtT = false;
      this.tanvecx = 0;
      this.tanvecy = 0;

      var allPoints = [];

      this.getAllPoints = function() {
        return [].concat(allPoints);
      }

      this.setAllPoints = function(pts) {
        allPoints.length = 0;
        for(var i = 0; i < pts.length; i++) allPoints.push(pts[i]);
      }

      this.resetiraj = function() {
        allPoints.length = 0;
        this.paint();
      }

      this.delLastPoint = function() {
        if(allPoints.length==0) return;
        allPoints.length = allPoints.length-1;
        this.paint();
      }

      function tVector(t, n) {
        var arr2 = [];
        for(var c = 0; c <= n; c++) {
          arr2[c] = Math.pow(t, n-c);
        }
        return arr2;
      }

      function tVectorDeriv(t, n) {
        var arr2 = [];
        for(var c = 0; c < n; c++) {
          arr2[c] = (n-c) * Math.pow(t, n-1-c);
        }
        arr2[n] = 0;
        return arr2;
      }

      function tMatrix(v) {
        var n = v.length;
        var arr = [];
        for(var r = 0; r < n; r++) {
          arr.push(tVector(v[r], n-1));
        }
        return math.matrix(arr);
      }

      function tMatrix2(v, t, n) {
        var arr = [];
        for(var r = 0; r < n; r++) {
          arr.push(tVector(v[r], n));
        }
        arr.push(tVectorDeriv(t, n));
        return math.matrix(arr);
      }

      function rasterizeCurve(ctx, samples, K) {
        var n = K.size()[0]-1;
        ctx.beginPath();
        for(var param = 0; param <= samples; param++) {
          var t = math.matrix([tVector(param/samples,n)]);
          var p = math.multiply(t,K);
          if(param==0) {
            ctx.moveTo(p.get([0,0]), p.get([0,1]));
          } else {
            ctx.lineTo(p.get([0,0]), p.get([0,1]));
          }
        }
        ctx.stroke();
      }

      function rasterizePolygon(ctx, points) {
        ctx.beginPath();
        for(var index = 0; index < points.length; index++) {
          var p = points[index];
          if(index==0) {
            ctx.moveTo(p[0], p[1]);
          } else {
            ctx.lineTo(p[0], p[1]);
          }
        }
        ctx.stroke();
      }

      function rasterizePolygonPoints(ctx, points) {
        ctx.beginPath();
        for(var index = 0; index < points.length; index++) {
          var p = points[index];
          ctx.moveTo(p[0]-5, p[1]-5);
          ctx.lineTo(p[0]+5, p[1]+5);
          ctx.moveTo(p[0]+5, p[1]-5);
          ctx.lineTo(p[0]-5, p[1]+5);
        }
        ctx.stroke();
      }

      function rasterizeTangent(ctx, pt, tv, showLine, showVector, scaleVector) {
        ctx.fillStyle = "#000000";
        ctx.beginPath();
        ctx.moveTo(pt[0]-5, pt[1]);
        ctx.lineTo(pt[0],   pt[1]-5);
        ctx.lineTo(pt[0]+5, pt[1]);
        ctx.lineTo(pt[0],   pt[1]+5);
        ctx.closePath();
        ctx.fill();

        var tnorm = Math.sqrt(tv[0]*tv[0]+tv[1]*tv[1]);
        var t = [tv[0], tv[1]];
        if(tnorm>0.0000001) {
          t[0] /= tnorm;
          t[1] /= tnorm;
        }
        if(showLine) {
          ctx.setLineDash([2,4]);
          ctx.beginPath();
          ctx.moveTo(pt[0]-1000*t[0], pt[1]-1000*t[1]);
          ctx.lineTo(pt[0]+1000*t[0], pt[1]+1000*t[1]);
          ctx.stroke();
          ctx.setLineDash([]);
        }
        var scaledTangentLength = 60;
        var dt = scaleVector ? [t[0]*scaledTangentLength, t[1]*scaledTangentLength] : tv;
        if(showVector) {
          var l = 20; var alpha = Math.PI/180.0*30;
          var dback = l*Math.cos(alpha);
          var dout = l*Math.sin(alpha);
          var dbackPt = [pt[0]+dt[0]-dback*t[0], pt[1]+dt[1]-dback*t[1]];
          var out1 = [dbackPt[0]-dout*t[1], dbackPt[1]+dout*t[0]];
          var out2 = [dbackPt[0]+dout*t[1], dbackPt[1]-dout*t[0]];
          ctx.beginPath();
          ctx.moveTo(pt[0], pt[1]);
          ctx.lineTo(pt[0]+dt[0], pt[1]+dt[1]);
          ctx.stroke();
          ctx.beginPath();
          ctx.moveTo(out1[0], out1[1]);
          ctx.lineTo(pt[0]+dt[0], pt[1]+dt[1]);
          ctx.stroke();
          ctx.beginPath();
          ctx.moveTo(out2[0], out2[1]);
          ctx.lineTo(pt[0]+dt[0], pt[1]+dt[1]);
          ctx.stroke();
        }
      }

      function showConstruction(ctx, points, t) {
        if(points.length<2) return;
        var arr = [];
        for(var i = 0; i < points.length-1; i++) {
          arr.push([(points[i+1][0]-points[i][0])*t+points[i][0], (points[i+1][1]-points[i][1])*t+points[i][1]]);
        }
        ctx.setLineDash([5,5]);
        ctx.beginPath();
        for(var index = 0; index < points.length-1; index++) {
          var p = points[index];
          ctx.moveTo(points[index][0], points[index][1]);
          ctx.lineTo(points[index+1][0], points[index+1][1]);
        }
        ctx.stroke();
        ctx.setLineDash([]);
        if(arr.length==1) {
          ctx.beginPath();
            ctx.moveTo(arr[0][0]-5, arr[0][1]);
            ctx.lineTo(arr[0][0], arr[0][1]-5);
            ctx.lineTo(arr[0][0]+5, arr[0][1]);
            ctx.lineTo(arr[0][0], arr[0][1]+5);
          ctx.closePath();
          ctx.fill();
        } else {
          ctx.beginPath();
          for(var index = 0; index < arr.length; index++) {
            var p = arr[index];
            ctx.moveTo(p[0]-5, p[1]-5);
            ctx.lineTo(p[0]+5, p[1]+5);
            ctx.moveTo(p[0]+5, p[1]-5);
            ctx.lineTo(p[0]-5, p[1]+5);
          }
          ctx.stroke();
        }
        showConstruction(ctx, arr, t);
      }

      function nfraction(n) {
        if(n==1) return [1];
        if(n==2) return [0, 1];
        var arr = [];
        for(var i = 0; i < n; i++) {
          arr.push(i/(n-1.0));
        }
        return arr;
      }

      this.paint1 = function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.fillStyle = "#ffffee";
        this.context.strokeStyle = "#aaaa00";

        var points = [].concat(allPoints);
        if(mouseInfo.isDown && this.addingEnabled) {
          // tu dalje...
          points.push([mouseInfo.pt.x, mouseInfo.pt.y]);
        }
        if(points.length>=2) {
          var B = bezierRMatrix(points.length-1);
          var R = math.matrix(points);
          if(this.curveApprox) {
            var BR = math.multiply(B,R);
            this.context.strokeStyle = "#ff0000";
            rasterizeCurve(this.context, 100, BR);
          }
          if(this.curveInterpol) {
            this.context.strokeStyle = "#aaaa00";
            rasterizeCurve(this.context, 100, math.multiply(math.inv(tMatrix(nfraction(points.length))),R));
          }
          if(this.showSimplerApprox) {
            this.context.strokeStyle = "#aaaaaa";
            var Bb = bezierRMatrix(points.length-2);
            var R1 = math.matrix(points.slice(0, points.length-1));
            rasterizeCurve(this.context, 100, math.multiply(Bb,R1));
            var R2 = math.matrix(points.slice(1, points.length));
            rasterizeCurve(this.context, 100, math.multiply(Bb,R2));
          }
          if(this.controlPolygon) {
            this.context.strokeStyle = "#0000ff";
            rasterizePolygon(this.context, points);
          }
          if(this.intControlPolygon || this.intControlPoints) {
            var Rcp = math.multiply(math.inv(math.multiply(tMatrix(nfraction(points.length)), B)), R).valueOf();
            if(this.intControlPolygon) {
              this.context.strokeStyle = "#ff00ff";
              rasterizePolygon(this.context, Rcp);
            }
            if(this.intControlPoints) {
              this.context.strokeStyle = "#ff00ff";
              rasterizePolygonPoints(this.context, Rcp);
            }
          }
          if(this.buildProc) {
            this.context.strokeStyle = "#aa00ff";
            this.context.fillStyle = "#aa00ff";
            showConstruction(this.context, points, this.tvalue);
          }
          if(this.showTangentAtT) {
            this.context.strokeStyle = "#0000ff";
            var BRt = math.multiply(B,R);
            var pt = math.multiply(math.matrix([tVector(this.tvalue, points.length-1)]), BRt).valueOf()[0];
            var tv = math.multiply(math.matrix([tVectorDeriv(this.tvalue, points.length-1)]), BRt).valueOf()[0];
            rasterizeTangent(this.context, pt, tv, this.showTangentLineAtT, this.showTangentVectorAtT, this.scaleTangentVectorAtT);
          }
        }
        if(this.controlPoints && points.length>0) {
          this.context.strokeStyle = "#000000";
          rasterizePolygonPoints(this.context, points);
        }
      }

      this.paint2 = function() {
        this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.context.fillStyle = "#ffffee";
        this.context.strokeStyle = "#aaaa00";

        var points = [].concat(allPoints);
        if(points.length>=2) {
          this.context.strokeStyle = "#000000";
          rasterizePolygonPoints(this.context, points);
          try {
            var B = bezierRMatrix(points.length);
            var frac = nfraction(points.length);
            var tm = tMatrix2(frac, this.tvalue, points.length);
            var P = math.matrix(points.concat([[this.tanvecx, this.tanvecy]]));
            var R = math.multiply(math.inv(math.multiply(tm,B)),P);

            var BR = math.multiply(B,R);
            this.context.strokeStyle = "#0000ff";
            rasterizeCurve(this.context, 100, BR);

            this.context.strokeStyle = "#ff0000";
            var pt = math.multiply(math.matrix([tVector(this.tvalue, points.length)]), BR).valueOf()[0];
            var tv = math.multiply(math.matrix([tVectorDeriv(this.tvalue, points.length)]), BR).valueOf()[0];
            rasterizeTangent(this.context, pt, tv, this.showTangentLineAtT, this.showTangentVectorAtT, this.scaleTangentVectorAtT);
          } catch(ex) {
            this.context.fillStyle = "#ff0000";
            var f = this.context.font;
            this.context.font="20px Georgia";
            this.context.fillText("Pogreška: " + ex.message,10,50);
          }
        }
      }

      this.paint = this.paint1;

      this.switchPaint = function(n) {
        this.paint = n == 1 ? this.paint1 : this.paint2;
      }

      var mouseInfo = {pt: null, isDown: false};

      function findClosestPoint(pts, pt, tolerance) {
        if(pts.length < 1) return -1;
        var dmin = Math.sqrt((pts[0][0]-pt[0])*(pts[0][0]-pt[0]) + (pts[0][1]-pt[1])*(pts[0][1]-pt[1]));
        var mi = 0;
        for(var i = 1; i < pts.length; i++) {
          var d = Math.sqrt((pts[i][0]-pt[0])*(pts[i][0]-pt[0]) + (pts[i][1]-pt[1])*(pts[i][1]-pt[1]));
          if(d < dmin) { 
            dmin = d; mi = i; 
          }
        }
        return dmin < tolerance ? mi : -1;
      }

      var me = this;
      this.canvas.addEventListener('mousedown', function(evt) {
        if(evt.button != 0 && evt.button != 2) return;
        var rect = me.canvas.getBoundingClientRect();
        var pos = {x: evt.clientX - rect.left, y: evt.clientY - rect.top};
        if(me.addingEnabled) {
          if(evt.button == 2) { allPoints.length = 0; me.paint(); return; }
          mouseInfo.pt = pos;
          mouseInfo.isDown = true;
          me.paint();
        } else {
          var ci = findClosestPoint(allPoints, [pos.x, pos.y], 10);
          if(ci==-1) return;
          mouseInfo.origpt = pos;
          mouseInfo.pt = pos;
          mouseInfo.ci = ci;
          mouseInfo.isDown = true;
          me.paint();
        }
      }, false);
      this.canvas.addEventListener('mousemove', function(evt) {
        if(!mouseInfo.isDown) return;
        if(evt.button != 0) return;
        var rect = me.canvas.getBoundingClientRect();
        var pos = {x: evt.clientX - rect.left, y: evt.clientY - rect.top};
        if(me.addingEnabled) {
          mouseInfo.pt = pos;
          me.paint();
        } else {
          if(mouseInfo.ci == -1) return;
          var dx = pos.x - mouseInfo.origpt.x;
          var dy = pos.y - mouseInfo.origpt.y;
          mouseInfo.origpt = pos;
          mouseInfo.pt = pos;
          allPoints[mouseInfo.ci][0] += dx;
          allPoints[mouseInfo.ci][1] += dy;
          me.paint();
        }
      }, false);
      this.canvas.addEventListener('mouseup', function(evt) {
        if(evt.button != 0 && evt.button != 2) return;
        if(evt.button == 2) { return; }
        if(!me.addingEnabled) {
          mouseInfo.ci = -1;
          return;
        }
        var rect = me.canvas.getBoundingClientRect();
        var pos = {x: evt.clientX - rect.left, y: evt.clientY - rect.top};
        mouseInfo.pt = pos;
        mouseInfo.isDown = false;
        allPoints.push([pos.x, pos.y]);
        me.paint();
      }, false);

  }

